<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JabatanController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
		$jabatans = \App\Jabatan::paginate(10);

    return view ('jabatans.index', ['jabatans' => $jabatans]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('jabatans.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
		$new_jabatan = new \App\Jabatan;
		
		$new_jabatan->name = $request->get('name');
		$new_jabatan->keterangan = $request->get('keterangan');
		$new_jabatan->gaji = $request->get('gaji');

		$new_jabatan->created_by = \Auth::user()->id;

		$new_jabatan->save();

		return redirect()->route('jabatans.create')->with('status', 'Data jabatan berhasil disimpan');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $jabatan = \App\Jabatan::findOrFail($id);

    return view('jabatans.edit', ['jabatan' => $jabatan]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $jabatan = \App\Jabatan::findOrFail($id);
		
		$jabatan->name = $request->get('name');
		$jabatan->keterangan = $request->get('keterangan');
		$jabatan->gaji = $request->get('gaji');

		$jabatan->updated_by = \Auth::user()->id;

		$jabatan->save();

		return redirect()->route('jabatans.edit', [$jabatan->id])->with('status', 'Data jabatan berhasil diedit');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
		$jabatan = \App\Jabatan::findOrFail($id);
		$jabatan->delete();

		return redirect()->route('jabatans.index')->with('status', 'Jabatan berhasil dihapus');

  }
}
