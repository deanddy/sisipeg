@extends('layouts.app')

@section('title')
  Tambah Data Pegawai
@endsection

@section('content')
  <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <div class="section-header-back">
              <a href="{{route('jabatans.index')}}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Tambah Data Jabatan</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></div>
              <div class="breadcrumb-item"><a href="{{route('jabatans.index')}}">Jabatan</div></a>
              <div class="breadcrumb-item active">Tambah Data Jabatan</div>
            </div>
          </div>

          <div class="section-body">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Silahkan isi data-data berikut ini</h4><br><br>
                  </div>

                  

                  <form
                    action="{{route('jabatans.store')}}"
                    method="POST">
                  
                    @csrf

                    <div class="card-body">
                      
                      @if(session('status'))
                      <div class="alert alert-success alert-dismissible show fade">
                        <div class="alert-body">
                          <button class="close" data-dismiss="alert">
                            <span>×</span>
                          </button>
                          {{session('status')}}
                        </div>
                      </div>
                      @endif

                      <div class="form-group">
                        <label for="name">Nama Jabatan / Divisi</label>
                        <input type="text" class="form-control" required="" name="name">
                      </div>
                      <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <textarea class="form-control" name="keterangan" id="keterangan"" required></textarea>
                      </div>
                      <div class="form-group mb-0">
                        <label for="">Gaji (Rp.)</label>
                        <input type="number" name="gaji" class="form-control" required>
                      </div>
                    </div>

                    <div class="card-footer text-right">
                      <button class="btn btn-primary">Simpan</button>
                    </div>

                  </form>
 
                </div>
              </div>
            </div>
          </div>

        </section>
      </div>
@endsection