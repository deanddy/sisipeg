@extends('layouts.app')

@section('title')
  Data Pegawai
@endsection

@section('content')
  <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Daftar Jabatan</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></div>
              <div class="breadcrumb-item active">Data Jabatan</div>
            </div>
          </div>

          <div class="section-body">
            <div class="row">
              <div class="col-12">
              
                @if(count($jabatans)>0)
                  <div class="card">
                    <div class="card-header">
                    <h4>Tabel Habatan</h4><br><br>
                      </div>

                      <div class="card-header">
                      
                        <a href="{{route('jabatans.create')}}" class="btn btn-icon icon-left btn-primary btn-sm">
                          <i class="far fa-edit"></i> Tambah data
                        </a>
                        
                      </div>

                    <div class="card-body">

                      @if(session('status'))
                      <div class="alert alert-success alert-dismissible show fade">
                        <div class="alert-body">
                          <button class="close" data-dismiss="alert">
                            <span>×</span>
                          </button>
                          {{session('status')}}
                        </div>
                      </div>
                      @endif

                      <div class="table-responsive">
                        <table class="table table-striped table-md">
                          <tbody><tr>
                            <th>ID</th>
                            <th>Nama Jabatan</th>
                            <th>Deskripsi / Keterangan</th>
                            <th>Gaji</th>
                            <th>Action</th>
                          </tr>

                          @foreach($jabatans as $jabatan)
                          <tr>
                            <td>{{$jabatan->id}}</td>
                            <td>{{$jabatan->name}}</td>
                            <td>{{$jabatan->keterangan}}</td>
                            <td>{{$jabatan->gaji}}</td>
    
                            <td>
                              <a href="{{route('jabatans.edit', [$jabatan->id])}}" class="btn btn-success btn-sm">Edit</a>

                              <form 
                                onsubmit="return confirm('Hapus jabatan ini secara permanen?')"
                                class="d-inline"
                                action="{{route('jabatans.destroy', [$jabatan->id])}}"
                                method="POST"
                                >

                                @csrf

                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm"></input>
                              </form>
                                
                            </td>

                          </tr>
                          @endforeach
                          </tbody>

                          <tfoot>
                            <tr>
                              <td colspan="10">
                                {{$jabatans->links()}}
                                
                                <!-- {{$jabatans->appends(Request::all())->links()}} -->
                              </td>
                            </tr>
                          </tfoot>
                          
                        </table>
                      </div>
                    </div>

                    {{-- <div class="card-footer text-right">
                      <nav class="d-inline-block">
                        <ul class="pagination mb-0">
                          <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1"><i class="fas fa-chevron-left"></i></a>
                          </li>
                          <li class="page-item active"><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a></li>
                          <li class="page-item">
                            <a class="page-link" href="#">2</a>
                          </li>
                          <li class="page-item"><a class="page-link" href="#">3</a></li>
                          <li class="page-item">
                            <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                          </li>
                        </ul>
                      </nav>
                    </div> --}}

                  </div>
                  
                @else
                  <div class="card">
                    <div class="card-header">
                      <h4>Data Masih Kosong</h4>
                    </div>
                    <div class="card-body">
                      <div class="empty-state" data-height="400" style="height: 400px;">
                        <div class="empty-state-icon">
                          <i class="fas fa-question"></i>
                        </div>
                        <h2>Tidak ada data yang ditemukan</h2>
                        <p class="lead">
                          Mohon maaf, silahkan isi data terlebih dahulu
                        </p>
                        <a href="{{route('jabatans.create')}}" class="btn btn-primary mt-4">Isi Data</a>
                      </div>
                    </div>
                  </div>
                @endif

            
              </div>
            </div>
          </div>

        </section>
      </div>
@endsection