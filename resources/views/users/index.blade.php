@extends('layouts.app')

@section('title')
  Data Pegawai
@endsection

@section('content')
  <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>Daftar Pegawai / User</h1>
            <div class="section-header-breadcrumb">
              <div class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></div>
              <div class="breadcrumb-item active">Data Pegawai</div>
            </div>
          </div>

          <div class="section-body">
            <div class="row">
              <div class="col-12">

                @if(count($users)>0)
                <div class="card">
                  <div class="card-header">
                    <h4>Tabel Pegawai / Users</h4><br><br>
                  </div>

                  <div class="card-header">
                   
                    <a href="{{route('users.create')}}" class="btn btn-icon icon-left btn-primary btn-sm">
                      <i class="far fa-edit"></i> Tambah data
                    </a>
                    
                  </div>

                  <div class="card-body p-0">

                    @if(session('status'))
                    <div class="alert alert-success alert-dismissible show fade">
                      <div class="alert-body">
                        <button class="close" data-dismiss="alert">
                          <span>×</span>
                        </button>
                        {{session('status')}}
                      </div>
                    </div>
                    @endif

                    <div class="table-responsive">
                      <table class="table table-striped table-md">
                        <tbody><tr>
                          <th>ID</th>
                          <th>Foto</th>
                          <th>Nama</th>
                          <th>Username</th>
                          <th>Email</th>                         
                          <th>NIK</th>
                          <th>Jenis Kelamin</th>
                          <th>Jabatan</th>                       
                          <th>Status Akun</th>
                          <th>Action</th>
                        </tr>

                        @foreach($users as $user)
                        <tr>
                          <td>{{$user->id}}</td>
                          <td>
                            @if($user->foto)
                            <img src="{{asset('storage/'.$user->foto)}}" width="70px"/> 
                              @else 
                                N/A
                              @endif
                          </td>
                          <td>{{$user->name}}</td>
                          <td>{{$user->username}}</td>
                          <td>{{$user->email}}</td>                          
                          <td>{{$user->nik}}</td>
                          <td>{{$user->jenis_kelamin}}</td>
                          <td>{{$user->jabatan}}</td>                          
                          <td>
                            @if($user->status == "ACTIVE")
                              <div class="badge badge-success">{{$user->status}}</div>
                            @else
                              <div class="badge badge-danger">{{$user->status}}</div>
                            @endif
                          </td>

                          <td>
                            <a href="#" class="btn btn-info btn-sm">Detail</a>
                            <a href="#" class="btn btn-success btn-sm">Edit</a>
                            <a href="#" class="btn btn-danger btn-sm">Delete</a>
                          </td>

                        </tr>
                        @endforeach
                        </tbody>

                        <tfoot>
                          <tr>
                            <td colspan="10">
                              {{$users->links()}}
                              
                              <!-- {{$users->appends(Request::all())->links()}} -->
                            </td>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>

                  <!-- <div class="card-footer text-right">
                    <nav class="d-inline-block">
                      <ul class="pagination mb-0">
                        <li class="page-item disabled">
                          <a class="page-link" href="#" tabindex="-1"><i class="fas fa-chevron-left"></i></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a></li>
                        <li class="page-item">
                          <a class="page-link" href="#">2</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                          <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                        </li>
                      </ul>
                    </nav>
                  </div> -->

                </div>

                @else
                <div class="card">
                  <div class="card-header">
                    <h4>Data Masih Kosong</h4>
                  </div>
                  <div class="card-body">
                    <div class="empty-state" data-height="400" style="height: 400px;">
                      <div class="empty-state-icon">
                        <i class="fas fa-question"></i>
                      </div>
                      <h2>Tidak ada data yang ditemukan</h2>
                      <p class="lead">
                        Mohon maaf, silahkan isi data terlebih dahulu
                      </p>
                      <a href="{{route('jabatans.create')}}" class="btn btn-primary mt-4">Isi Data</a>
                    </div>
                  </div>
                </div>
                @endif
              </div>
            </div>
          </div>

        </section>
      </div>
@endsection